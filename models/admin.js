/**
 * New node file
 */
var mongoose = require('mongoose');
var AdminSchema = require("../db/AdminSchema");

function loginConUsuarioContrasena(usuario,contrasena,callbackLogin){
	console.log("admin.js loginConUsuarioContrasena");
	console.log(usuario);
	console.log(contrasena);
	AdminSchema.findOne({usuario:usuario,contrasena:contrasena},function(err,admins){
		console.log("Buscando Admin");
		if(err){
			callbackLogin(false);
		} else {
	    	if(admins == null){
	    		console.log("No hay admin con esos datos");
	    		callbackLogin(false,null);
	    	} else {
	    		console.log("Admin encontrado");
	    		callbackLogin(true,admins);
	    	}
		}
 
	});
}

module.exports = {
	loginConUsuarioContrasena:loginConUsuarioContrasena
};