/**
 * New node file
 */
var mongoose = require('mongoose');

var ActualizacionSchema = require("../db/ActualizacionSchema");

function obtener(req,callbackObtener){
	console.log("actualizacion.js obtener");
	ActualizacionSchema.find(function(err, actualizaciones) {
		console.log("Buscando actualizaciones");
		if(err){
			res.send(err);
		} else {
	    	if(actualizaciones.length==0){
	    		console.log("Sin actualizaciones");
	    		callbackObtener(null);
	    	} else {
	    		console.log("Total actualizaciones " + actualizaciones.length);
	    		callbackObtener(actualizaciones);
	    	}
		}
    });
}

function guardar(callbackGuardar){
	console.log("actualizacion.js guardar");
	var actualizacion = new ActualizacionSchema();
	actualizacion.save(function(err, ActualizacionSchema) {
		if (err){ 
			console.log("Actualizacion no guardado. Error " + err);
			callbackGuardar(false);
		} else {
			console.log("Actualizacion guardada");
			callbackGuardar(true);
		}
	});
}

function borrar(callbackBorrar){
	console.log("actualizacion.js borrar");
	var actualizacionSCH = new ActualizacionSchema();
	ActualizacionSchema.find(function(err, actualizaciones) {
		if(actualizaciones.size!=0){
			for(var i =0;i<actualizaciones.length;i++){
				var actualizacion = actualizaciones[i];
				actualizacion.remove({_id:actualizacion._id},function(err){
				});
				if(i==actualizacion.length-1){
					callbackBorrar(true);
				}			
			}
		} else {
			callbackBorrar(false);
		}
	});
}
exports.obtener = obtener;
exports.guardar = guardar;
exports.borrar = borrar;