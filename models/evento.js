/**
 * New node file
 */
var mongoose = require('mongoose');

var EventoSchema = require("../db/EventoSchema");
var porPagina = 10;

var server = "http://www.mimanadadondeesta.cl/elevento/apiElevento";

function obtener(pag,callbackObtener){
	console.log("evento.js obtener");

	EventoSchema.find(function(err, eventos) {
		console.log("Buscando Eventos");
		if(err){
			callbackObtener(null,null);
		} else {
	    	if(eventos.length==0){
	    		console.log("Sin eventos");
	    		callbackObtener(null);
	    	} else {
	    		console.log("Total Eventos " + eventos.length);
	    		var totalPaginas = Math.ceil(eventos.length/porPagina);
	    		
	    		var losEventos = [];
		    	for (var i = 0;i<eventos.length;i++){
		    		var unEvento = eventos[i];
		    		var ruta = server+unEvento.imagen;
		    		var rutaMini = server+unEvento.imagenMini;
		    		unEvento.imagen = ruta;
		    		unEvento.imagenMini = rutaMini;
		    		losEventos.push(unEvento);
		    	}
	    		
	    		callbackObtener(totalPaginas,losEventos);
	    	}
		}
    }).limit(porPagina).skip(pag);
}

function obtenerConID(id,callbackBuscar){
	console.log("evento.js obtenerConID: " + id);
	EventoSchema.findOne({_id:id},function(err, unEvento) {
		console.log("Buscando Eventos");
		if(err){
    		callbackBuscar(null);
		} else {
	    	if(unEvento == null){
	    		console.log("Sin eventos");
	    		callbackBuscar(null);
	    	} else {
	    		var ruta = server+unEvento.imagen;
	    		var rutaMini = server+unEvento.imagenMini;
	    		unEvento.imagen = ruta;
	    		unEvento.imagenMini = rutaMini;
		    	console.log(unEvento);
	    		callbackBuscar(unEvento);
	    	}
		}
    });	
}
function obtenerUltimo(callbackBuscar){
	console.log("evento.js obtenerUltimo");
	EventoSchema.find({},function(err, evento) {
		console.log("Buscando Eventos");
		if(err){
			callbackBuscar(null);
	    } else {
	    	if(evento == null){
	    		console.log("Sin eventos");
	    		callbackBuscar(null);
	    	} else {
	    		console.log("Evento ultimo encontrado");
	    		console.log(evento.length);
	    		var unEvento = evento[0];
	    		var ruta = server+unEvento.imagen;
	    		var rutaMini = server+unEvento.imagenMini;
	    		unEvento.imagen = ruta;
	    		unEvento.imagenMini = rutaMini;
	    		callbackBuscar(unEvento);
	    	}
		}
    }).sort({$natural:-1}).limit(1);
}

function guardar(nombre,descripcion,imagen,imagenMini,fecha,callbackGuardar){
	console.log("evento.js guardar: " + nombre);
	var eventoGuardable = new EventoSchema({nombre:nombre,descripcion:descripcion,imagen:"",imagenMini:"",fecha:fecha,destacado:0,valid:1});
	eventoGuardable.toString();	
	eventoGuardable.save(function(err, EventoSchema) {
		if (err){ 
			console.log("Evento no guardado. Error " + err);
			callbackGuardar(false,null);
			return console.error(err);
		} else {
			console.log("Evento guardado");
			EventoSchema.toString();			
			callbackGuardar(true,EventoSchema.id);				

		}
	});
}

function actualizar(id,nombre,descripcion,imagen,imagenMini,fecha,callbackUpdate){
	console.log("evento.js actualizar");
	EventoSchema.findOneAndUpdate({_id:id},{nombre:nombre,descripcion:descripcion,imagen:imagen,imagenMini:imagenMini,fecha:fecha}, {upsert:false}, function(err, evento){
		if(err){
			console.log(err);
			callbackUpdate(false);
		} else {
			console.log(evento);
			callbackUpdate(true);
		}				
	});
}

/*
module.exports = {
	obtener: obtener,
};
*/
exports.obtener = obtener;
exports.obtenerConID = obtenerConID;
exports.obtenerUltimo = obtenerUltimo;
exports.guardar = guardar;
exports.actualizar = actualizar;
exports.eliminar