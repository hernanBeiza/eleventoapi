/**
 * New node file
 */

var mongoose = require('mongoose');
var TokenSchema = require("../db/TokenSchema");

function guardar(udid,plataforma,callbackGuardar){
	console.log("token.js guardar: " + udid);
	
	buscarConUDID(udid,function(estado){
		if(!estado){
			var tokenGuardar = new TokenSchema({udid:udid,plataforma:plataforma});
			tokenGuardar.save(function(err, Token) {
				if (err){ 
					console.log("Error. Token no guardado. Error " + err);
					callbackGuardar(false);
				} else {
					console.log("Token guardado OK");			
					callbackGuardar(true);
				}
			});
		} else {
			console.log("Token ya existe");
			callbackGuardar(false);			
		}		
	});
	
}

function buscarConUDID(udid,callbackBuscar){
	TokenSchema.find({udid:udid},function(err, tokens) {
		if(err){
			res.send(err);
		} else {
	    	if(tokens.length==0){
	    		callbackBuscar(false);
	    	} else {
	    		callbackBuscar(true);
	    	}
		}
    });	
}

function obtenerIOS(callbackObtener){
	TokenSchema.find({plataforma:2},function(err, tokens) {
		if(err){
			res.send(err);
		} else {
	    	if(tokens.length==0){
	    		callbackObtener(null);
	    	} else {
	    		callbackObtener(tokens);
	    	}
		}
    });	
	
}

module.exports = {
	guardar:guardar,
	obtenerIOS:obtenerIOS,
}