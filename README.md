# EleventoAPI
API para la aplicación Móvil Elevento.


## Uso de la api
### Para obtener/guardar/editar eventos

	app.get("/eventos"); //Obtener una lista de eventos
	app.get("/eventos/:eventoid"); //Obtener un evento específico 
	app.post("/eventos"); //Guardar un evento

	app.put("/eventos/:eventoid"); //Actualizar un evento específico

	app.get("/actualizaciones") //Obtener actualizaciones.
	app.get("/actualizacionesEnviar"); //En caso de existir actualizaciones, enviará las notificaciones

### Para registrar los tokens de los dispositivos
	app.post("/token",tokenController.guardar);
### Para entrar al sistema de administración
	app.post("/admin",adminController.login);

### Tools

Created with [Nodeclipse](https://github.com/Nodeclipse/nodeclipse-1)
 ([Eclipse Marketplace](http://marketplace.eclipse.org/content/nodeclipse), [site](http://www.nodeclipse.org))   

- Nodeclipse is free open-source project that grows with your contributions.
- moongose: Modelador de objetos para MongoDB
- express: Servidor
- apn: Paquete de notificacioens
- SublimeText: Editor de texto
