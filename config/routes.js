/**
 * Encargado de exportar las rutas
 */
//Importar los módulos requeridos
var indexController = require ("../controllers/indexController");
var adminController = require ("../controllers/adminController");

var eventoController = require ("../controllers/eventoController");
var actualizacionController = require ("../controllers/actualizacionController");
var tokenController = require ("../controllers/tokenController");

//obtenemos el app contenido en app.js
module.exports = function(app){
	app.all('/', function(req, res, next) {
		res.header('Access-Control-Allow-Origin', '*');
		res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
		res.header('Access-Control-Allow-Headers', 'Content-Type');
		/*
		res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
		res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
		res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
		*/
		next();
	});
	//eventos
	app.all('/*', function(req, res, next) {
		res.header('Access-Control-Allow-Origin', '*');
		res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
		res.header('Access-Control-Allow-Headers', 'Content-Type');
		next();
	});
	/*
	app.all('/eventos', function(req, res, next) {
		res.header('Access-Control-Allow-Origin', '*');
		res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
		res.header('Access-Control-Allow-Headers', 'Content-Type');
		next();
	});
	app.all('/eventos/:eventoid', function(req, res, next) {
		res.header('Access-Control-Allow-Origin', '*');
		res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
		res.header('Access-Control-Allow-Headers', 'Content-Type');
		next();
	});
	*/

	app.get("/",indexController.saludar);
	//eventos
	app.get("/eventos",eventoController.obtener);
	app.get("/eventos/:eventoid",eventoController.obtenerConID);
	app.post("/eventos",eventoController.guardar);
	//soportar options y put para el mismo tipo de evento (chrome)
	app.options("/eventos/:eventoid",eventoController.actualizar);
	app.put("/eventos/:eventoid",eventoController.actualizar);
	//actualizaciones
	app.get("/actualizaciones",actualizacionController.obtener);
	app.get("/actualizacionesEnviar",actualizacionController.enviar);
	//registro de tokens
	app.post("/token",tokenController.guardar);
	//admin del sistema
	app.post("/admin",adminController.login);
	
};