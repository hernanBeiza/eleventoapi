/**
 * New node file
 */

var token = require("../models/token");

function guardar(req,res) {
	console.log("tokenController guardar();");
	var udid = req.body.udid;
	var plataforma = req.body.plataforma;
	var guardar = true;
	var errores = "Te faltó:"
	if(udid == undefined){
		guardar = false;
		errores+="\nUDID";
	}
	if (plataforma == undefined){
		guardar = false;
		errores+="\nPlataforma";
	}
	if(guardar){
		token.guardar(udid,plataforma,function(estado){
			//console.log(eventos);
			if(estado){			
			    res.json({result:1, mensaje:"Token guardado con éxito"});
			} else {
				res.json({result:2, mensaje: "Error al guardar el token"})
			}		
		});		
	} else {
		res.json({result:2, mensaje: errores})
	}
}

function obtenerIOS(req,res){
	
}

module.exports = {
	guardar:guardar,
	obtenerIOS:obtenerIOS,
};