const c = require('chalk');

var evento = require("../models/evento");
var actualizacion = require("../models/actualizacion");

function obtener(req,res) {
	console.log(c.cyan('eventoController: obtener();'));
	var pag = req.query.pag;
	if(pag==undefined){
		pag = 0
	} else if(pag==0){
		pag = 0;
	} else if(pag==1){
		pag = 0;
	} else {
		pag = (pag-1)*10;
	}
	evento.obtener(pag,function(paginas,eventos){
		if(eventos != null){
		    res.json({result:true,paginas:paginas,eventos:eventos,mensaje:"Eventos encontrados"});
		} else {
			res.json({result:false,eventos:null,mensaje: "No hay eventos"})
		}
	});
}

function obtenerConID(req,res){
	console.log(c.cyan('eventoController: obtenerConID();'));
	
	var id = req.params.eventoid;
	console.log("id " + id);
	var enviar = true;
	if(id==undefined){
		enviar  = false;
	}
	if(enviar){		
		evento.obtenerConID(id,function(evento){
			if(evento){
			    res.json({result:true, evento:evento, mensaje:"Evento encontrado"});
			} else {
				res.json({result:false, evento:null,mensaje: "No hay eventos"})
			}		
		});
	} else{
		res.json({result:false, mensaje: "Debes enviar el id del evento"})
	}

}

function guardar(req,res){
	console.log(c.cyan('eventoController: guardar();'));

	var enviar = true;
	var errores = "Te faltó:";
	if(req.body){
		var nombre = req.body.nombre;
		var descripcion = req.body.descripcion;
		var imagen = req.body.imagen;
		var fecha = req.body.fecha;
		
		console.log("nombre " + nombre);
		console.log("descripcion" + descripcion);
		if(imagen==undefined){
			console.log("imagen undefined");
		}
		console.log("fecha " + fecha);
		
		
		if(nombre == undefined){
			enviar = false;
			errores+="\n Nombre";
		}
		if(descripcion == undefined){
			enviar = false;
			errores+="\n Descripción";
		}
		if(imagen == undefined){
			enviar = false;
			errores+="\n Imagen";
		}
		if(fecha == undefined){
			enviar = false;
			errores+="\n Fecha";
		}
	}

	if(enviar){
		evento.guardar(nombre,descripcion,imagen,"",fecha,function(estado,eventoID){
			console.log(estado);
			if(estado){
				//guardar la foto en este formato porque la imagen se editó en el formulario
				var base64Data = imagen.replace(/^data:image\/png;base64,/, "");
				guardarImagen(base64Data,function(estado,params){
					if(estado) {
						//actualizar una vez hecho todo el trabajo con las imágenes
			    		evento.actualizar(eventoID,nombre,descripcion,params.rutaDB,params.rutaDBMINI,fecha,function(estado){
				    		if(estado){				    			
				    			//agregar actualizacion acá
				    			actualizacion.guardar(function(estado){
				    				if(estado){
					    				console.log("Actualizacion guardada");				    					
				    				} else {
				    					console.log("Actualización no guardada");
				    				}
				    			});				    			
							    res.json({result:true, mensaje: "Evento guardado correctamente."});  
				    		} else {
								res.json({result:false, mensaje: "El evento no se guardó. Error al actualizar"});
				    		}				    		
				    	});				    		
					} else {
						res.json({result:false, mensaje: "El evento no se guardó. Hubo un problema con el guardado de la imagen."});					
					}					
				});
		
			} else {
				res.json({result:false, mensaje: "El evento no se guardó."})				
			}			
		});		
	} else {
		console.log(errores);
		res.json({result:false, mensaje: errores})
	}	
}

function actualizar(req,res){
	console.log(c.cyan("eventoController actualizar(); "+req.params.eventoid));

	var eventoID = req.params.eventoid;
	var nombre = req.body.nombre;
	var descripcion = req.body.descripcion;
	var imagen = req.body.imagen;
	var fecha = req.body.fecha;
	console.log("nombre " + nombre);
	console.log("descripcion " + descripcion);
	if(imagen==undefined){
		console.log("imagen undefined");
	}
	//console.log("imagen " + imagen);
	console.log("fecha " + fecha);
	
	var enviar = true;
	var errores = "Te faltó:";
	if(eventoID == undefined){
		enviar = false;
		errores+="\n ID";
	}
	if(nombre == undefined){
		enviar = false;
		errores+="\n Nombre";
	}
	if(descripcion == undefined){
		enviar = false;
		errores+="\n Descripción";
	}
	if(imagen == undefined){
		enviar = false;
		errores+="\n Imagen";
	}
	if(fecha == undefined){
		enviar = false;
		errores+="\n Fecha";
	}
	if(enviar){
		evento.obtenerConID(eventoID,function(eventoEncontrado){
			if(eventoEncontrado!=null){
				console.log("evento encontrado con id" + eventoID);
				//console.log(eventoEncontrado);
				var base64Data = imagen.replace(/^data:image\/png;base64,/, "");
				guardarImagen(base64Data,function(estado,params){
					if(estado) {
						//actualizar una vez hecho todo el trabajo con las imágenes
			    		evento.actualizar(eventoID,nombre,descripcion,params.rutaDB,params.rutaDBMINI,fecha,function(estado){
				    		if(estado){
							    res.json({result:true, mensaje: "Evento guardado correctamente."});  
				    		} else {
								res.json({result:false, mensaje: "El evento no se guardó. Error al actualizar"});
				    		}				    		
				    	});				    		
					} else {
						res.json({result:false, mensaje: "El evento no se guardó. Hubo un problema con el guardado de la imagen."});					
					}					
				});
			} else {
				res.json({result:false, mensaje: "No se encontró evento con ese id"});
			}
		});						
	
	} else {
		console.log(errores);
		res.json({result:false, mensaje: errores});
	}
}

//Retorna un objeto con: estado, nombreArchivo, rutaFinal, rutaFinalMINI, rutaDB, rutaDBMINI
function guardarImagen(base64Data,callbackImagenGuardar){
	//TODO buscar como ocupar lwip o buscar una alternativa
	//var lwip = require('lwip');
	var multer = require('multer');
	var shortid = require('shortid');
	var fs = require("fs");

	var config = require("./../config/config");
	var nombreArchivo = shortid.generate()+".png";
	var rutaFinal = config.carpetas.img+nombreArchivo;
	var rutaFinalMINI = config.carpetas.mini+nombreArchivo;
	console.log(c.yellow(rutaFinal),c.yellow(rutaFinalMINI));
	var rutaDB = nombreArchivo;
	var rutaDBMINI =nombreArchivo;
	console.log(c.yellow(rutaFinal),c.yellow(rutaFinalMINI));

	var estado = true;
	fs.writeFile(rutaFinal, base64Data, 'base64', function(err) {
		if(err){
			console.log(err);
	    	estado = false;
	    	errores +="Error al guardar la imagen";
			res.json({result:2, mensaje: "La imagen del evento no se guardó."})
	    } else {
	    	console.log("Guardado imagen grande ok");
	    	var params = {nombreArchivo:nombreArchivo,rutaFinal:rutaFinal,rutaFinalMINI:rutaFinalMINI,rutaDB:rutaDB,rutaDBMINI:rutaDBMINI}
	    	callbackImagenGuardar(true,params);

	    	//generar miniatura del archivo recién subido
	    	/*
	    	lwip.open(rutaFinal,function(err,image){
		    	if(err) {
		    		console.log("imagen no abierta");
		    	} else {			    		
			    	console.log("imagen abierta");				
			    	var maxW = 200,
		    	    maxH = 200,
		    	    ratio = Math.min(maxW / image.width(), maxH / image.height());
	
			    	// resized to be in square 100x100
			    	var ratio = 100 / Math.min(image.width(), image.height());
	
			    	// if you need it be applied only for downscaling
			    	ratio = ratio < 1 ? ratio : 1;

		    		image.scale(ratio, function(err, image){
			    	//image.resize(200, 200, function(err, image){
			    		if(err) {
			    			console.log("error al obtener la imagen a miniaturizar");
			    		} else {
			    			console.log("imagen a miniaturizar obtenida correctamente");
				    	    // encode resized image to jpeg and get a Buffer object
			    			image.toBuffer('jpg', {quality: 90}, function(err, buffer){
			    			    // Send buffer over the network, save to disk, etc.
				    	    	if(err){
				    	    		console.log("error al generar la miniatura");
				    	    	} else {
				    	    		console.log("miniatura ok");
				    	    		
				    				require("fs").writeFile(rutaFinalMINI, buffer, function(err) {
				    					if(err){
				    						console.log(err);
				    				    	console.log("Save mini error");
				    				    	estado = false;
				    				    	var params = null;
				    				    	callbackImagenGuardar(estado,params);
				    				    } else {
				    				    	console.log("Save mini ok ok");
				    				    	estado = true;
				    				    	var params = {nombreArchivo:nombreArchivo,rutaFinal:rutaFinal,rutaFinalMINI:rutaFinalMINI,rutaDB:rutaDB,rutaDBMINI:rutaDBMINI}
				    				    	callbackImagenGuardar(estado,params);
				    				    }
				    				});
				    	    	}
				    	    });
			    		}
			    	});	   
		    	}		    	
	    	});
	    	*/
	    		    	
		}
	});
}

module.exports = {
	obtener:obtener,
	obtenerConID:obtenerConID,
	guardar:guardar,
	actualizar:actualizar,
};