/**
 * New node file
 */
var admin = require("../models/admin");

function login(req,res) {
	
	var usuario = req.body.usuario;
	var contrasena = req.body.contrasena;

	console.log("usuario " + usuario);
	console.log("contrasena " + contrasena);
	
	var enviar = true;
	var errores = "Te faltó:"
	if(usuario==undefined){
		enviar  = false;
		errores+="\nusuario";
	}
	
	if(contrasena==undefined){
		enviar  = false;
		errores+="\ncontrasena";
	}

	if(enviar){		
		admin.loginConUsuarioContrasena(usuario,contrasena,function(estado,admin){
			console.log(estado);
			console.log(admin);
			if(estado){
			    res.json({result:1, mensaje:"Bienvenido " + admin.nombre});
			} else {
				res.json({result:2, mensaje: "No hay administrador con esos datos"})
			}		
		});
	} else{
		res.json({result:2, mensaje: errores})
	}

}

module.exports = {
	login:login,
};