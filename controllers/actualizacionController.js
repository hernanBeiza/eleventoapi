var actualizacion = require("../models/actualizacion");


var EventoSchema = require("../db/EventoSchema");
var evento = require("../models/evento");
var token = require("../models/token");

function obtener(req,res){
	console.log("actualizacionController obtener();");
	
	actualizacion.obtener(req,function(actualizaciones){
		if(actualizaciones != null){
		    res.json({result:1, actualizaciones:actualizaciones});
		} else {
			res.json({result:2, mensaje: "No hay actualizaciones"})
		}		
	});	
}

function enviar(req,res){
	console.log("actualizacionController enviar();");
	//test
	//enviarIOS("05d6b01ed5c42002bdecd80f04f8e9630b09ae8fb2049684dd69673278eda1c7","566c7b6e4c0fa7160fa8fc78");

	actualizacion.obtener(req,function(actualizaciones){
		if(actualizaciones != null){
			//Obtener el último evento
			var ultimo = evento.obtenerUltimo(function(elevento){
				console.log(elevento);
				//Obtener Tokens de usuarios a enviar notificacion
				token.obtenerIOS(function(tokens){
					//Enviar notificaciones a todos los tokens, con el id del evento agregado			
					//enviarIOS("05d6b01ed5c42002bdecd80f04f8e9630b09ae8fb2049684dd69673278eda1c7",evento._id);

					for(var i = 0; i<tokens.length;i++){
						var unToken = tokens[i];
						enviarIOS(unToken.udid,elevento._id);
					}
					
					//Borrar la actualización de la db
					actualizacion.borrar(function(estado){
						if(estado){
						    res.json({result:1, mensaje:"Notificaciones enviadas correctamente :)"});					
						} else {
							res.json({result:2, mensaje:"Notificaciones no borradas. Error al borrar"});					
						}
					});
						
				});
								
				
			});
		    
		} else {
			res.json({result:2, mensaje: "No hay notificaciones"})
		}		
	});
}

function enviarIOS(token,eventoID){
	console.log("enviarIOS " + token);
	var apn = require('apn');
    var options = {cert:"../certs/cert.pem",key:"../certs/key.pem",passphrase:"elevento" };
    var apnConnection = new apn.Connection(options);
    var myDevice = new apn.Device(token);

    var note = new apn.Notification();
    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
    note.badge = 1;
    note.sound = "default";
    note.alert = "¡Hay nuevos eventos!";
    note.payload = {'eventoID': eventoID};
    apnConnection.pushNotification(note, myDevice);
}

module.exports = {
	obtener:obtener,
	enviar:enviar,
};