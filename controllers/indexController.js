// Se importan módulos en caso necesario
//var saludador = require("../models/saludador");
var index = require("../models/index");

function saludar(req,res) {
	var pjson = require('./../package.json');
    res.json({mensaje:"Bienvenido a la API de Elevento v"+pjson.version});
}

module.exports = {
	saludar:saludar,
};