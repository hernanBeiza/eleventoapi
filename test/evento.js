// Revisar para más detalles de los mensajes
// https://scotch.io/tutorials/test-a-node-restful-api-with-mocha-and-chai
// During the test the env variable is set to test
//
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let EventoSchema = require('../db/EventoSchema');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../appElevento');
let should = chai.should();

chai.use(chaiHttp);


describe('Evento', () => {
  //Borrar la DB en cada inicio de prueba
  /*
  beforeEach((done) => { 
    EventoSchema.remove({}, (err) => { 
      console.log(err);
      done();         
    });     
  });
  */

  // Probar la obtención de los eventos
  describe('/GET eventos', () => {
      it('Debería obtener todos los eventos', (done) => {
        chai.request(server)
            .get('/eventos')
            .end((err, res) => {
              //console.log(err);
              //console.log(res.body.result);
              //console.log(res.body.mensaje);
              console.log(res.body);
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('result');
              res.body.should.have.property('mensaje');
              res.body.should.have.property('eventos');

              res.body.result.should.be.a('boolean');
              res.body.mensaje.should.be.a('string');

              //res.body.result.should.be.eql(true);

            done();
          });
      });
  });

  // Obtener un evento específico
  describe('/GET/ eventos/:eventoid', () => {
      it('Debería obtener un evento según el eventoid', (done) => {
        chai.request(server)
            .get('/eventos/5a28c9d459dbf8269e275e2e')
            .end((err, res) => {
              //console.log(err);
              //console.log(res.body.result);
              //console.log(res.body.mensaje);
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('result');
              res.body.should.have.property('mensaje');
              res.body.should.have.property('evento');

              res.body.result.should.be.a('boolean');
              res.body.mensaje.should.be.a('string');
              res.body.evento.should.be.a('object');

              //res.body.result.should.be.eql(true);

            done();
          });

      });
  });

  // Probar la inserción de un evento
  describe('/POST eventos', () => {
      it('Debería guardar un evento con todos los datos', (done) => {
        let evento = {
            nombre: "Evento de prueba",
            descripcion: "Este es el evento de prueba usando chai",
            imagen: "una imagen en 64",
            fecha:"2017-10-21"
        }
        chai.request(server)
            .post('/eventos')
            .send(evento)
            .end((err, res) => {
              //console.log(err);
              //console.log(res.body.result);
              //console.log(res.body.mensaje);
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('result');
              res.body.should.have.property('mensaje');

              res.body.result.should.be.a('boolean');
              res.body.mensaje.should.be.a('string');

              //res.body.result.should.be.eql(true);

            done();
          });
      });
  });
  
  // Probar actualizar un evento
  describe('/PUT eventos/:eventoid', () => {
    var eventoid = "asdf";
      it('Debería actualizar el evento según eventoid', (done) => {
        let evento = {
            nombre: "Evento de prueba",
            descripcion: "Este es el evento de prueba usando chai",
            imagen: "una imagen en 64",
            fecha:"2017-10-21"
        }
        chai.request(server)
            .put('/eventos/'+eventoid)
            .send(evento)
            .end((err, res) => {
              //console.log(err);
              console.log(res.body);
              //console.log(res.body.mensaje);
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('result');
              res.body.should.have.property('mensaje');

              res.body.result.should.be.a('boolean');
              res.body.mensaje.should.be.a('string');
              
              //res.body.result.should.be.eql(true);

            done();
          });
      });
  });

});