var express = require('express')

var routes = require("./config/routes");
var http = require('http');
var path = require('path');
var bodyParser = require('body-parser');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
	app.use(express.errorHandler());
}

// Crear Carpetas
var fs = require("fs");
var config = require("./config/config");
console.log(config);
var carpetas = config.carpetas;
for (key in carpetas){
	var carpeta = carpetas[key];
	if (!fs.existsSync(carpeta)){
		console.log("Crear "+carpeta);
	    fs.mkdirSync(carpeta);
	}
}

routes(app);

console.log("environment",app.get('env'));

http.createServer(app).listen(app.get('port'), function(){
	console.log('Express server listening on port ' + app.get('port'));
});

// for testing
module.exports = app; 