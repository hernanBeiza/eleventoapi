/**
 * Objeto de la DB, esquema
 */
var mongoose = require('mongoose');
var db = require('./DB');
var Schema = mongoose.Schema;

var ActualizacionSchema   = new Schema();

ActualizacionSchema.methods.toString = function(){
	console.log("ActualizacionSchema.toString()");
}

module.exports = db.model('Actualizacion', ActualizacionSchema,'actualizacion');