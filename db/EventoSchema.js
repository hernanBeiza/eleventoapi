/**
 * Objeto de la DB, esquema
 */
var mongoose = require('mongoose');
var db = require('./DB');
var Schema = mongoose.Schema;

var EventoSchema   = new Schema({
    nombre: String,
    descripcion: String,
    imagen: String,
    imagenMini: String,
    fecha: String,
    destacado: Number,
    valid: Number,
});

EventoSchema.methods.toString = function(){
	console.log("EventoSchema.toString()");
	console.log("id " + this.id);
	console.log("nombre " + this.nombre);	
	console.log("descripcion " + this.descripcion);	
	console.log("imagen " + this.imagen);	
	console.log("imagenMini " + this.imagenMini);	

	console.log("fecha " + this.fecha);	
	console.log("destacado " + this.destacado);	
	console.log("valid " + this.valid);	
}

//module.exports = mongoose.model('Evento', EventoSchema,'evento');
module.exports = db.model('Evento', EventoSchema,'evento');