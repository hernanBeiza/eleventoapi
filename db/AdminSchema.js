/**
 * Objeto de la DB, esquema
 */
var mongoose = require('mongoose');
var db = require('./DB');
var Schema = mongoose.Schema;

var AdminSchema = new Schema({
    _id:String,
	usuario: String,
	contrasena: String,
    nombre: String,
});

AdminSchema.methods.toString = function(){
	console.log("AdminSchema.toString()");
	console.log("id " + this._id);
	console.log("usuario " + this.usuario);	
	console.log("contrasena " + this.contrasena);	
	console.log("nombre " + this.nombre);	
};

module.exports = db.model('Admin', AdminSchema,'admin');