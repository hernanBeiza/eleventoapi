var mongoose = require('mongoose');

// Obtiene la confguración según el NODE_ENV o environment
let config = require('config'); 
var db;
// DB options
let options = { 
	server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }, 
	replset: { socketOptions: { keepAlive: 1, connectTimeoutMS : 30000 } } 
}; 

//DB connection      
//var rutaDB = "mongodb://localhost:27017/eleventodb";
//db = mongoose.createConnection(rutaDB);
db = mongoose.createConnection(config.DBHost);
db.on('error', function(err){
	if(err) throw err;
});
db.once('open', function callback () {
	console.info('Mongo db connected successfully');
});

module.exports = db;