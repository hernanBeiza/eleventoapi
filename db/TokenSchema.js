/**
 * Objeto de la DB, esquema
 */
var mongoose = require('mongoose');
var db = require('./DB');
var Schema = mongoose.Schema;

var TokenSchema   = new Schema({
    udid: String,
    plataforma: String,
});

TokenSchema.methods.toString = function(){
	console.log("EventoSchema.toString()");
	console.log("udid " + this.udid);
	console.log("plataforma " + this.plataforma);	
};

module.exports = db.model('Token', TokenSchema,'token');